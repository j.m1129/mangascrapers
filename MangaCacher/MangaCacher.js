var redis = require("then-redis"),
    Q = require("q");

var MangaCacher = function(config){

var self = this;
if(config=="undefined" || config==null){

  self.config = {
    host:null,
    port:null,
    database:null,
    password:null
  }
}
else {
  //Connection configs
  self.config= config;
}


self.GetCacheIfExists = function(key){
	var deffered = Q.defer();
  var configs = self.config;
    var client = redis.createClient({
		   host:configs.host,
		   port:configs.port,
       database:configs.database,
       password:configs.password
		});

    client.connect().then(function(){

	    	client.get(key).then(function(value){
			if(value==null){
	            deffered.resolve("-1"); //No value found for the key
			}
			else {
				  deffered.resolve(value);
			}
	        client.quit();
		});

    },function(error){
   throw new Errot(error);
  });



	return deffered.promise;
};

self.CacheSearch = function(key,value){
	var deffered = Q.defer();
  var config = self.config;
	var client = redis.createClient({
		   host:config.host,
       port:config.port,
       database:config.database,
       password:config.password
		});

   client.connect().then(function(){

	    	client.set(key,value).then(function(value){

	        if(value!=null){
	        	deffered.resolve(value);
	        }
	        client.quit();
		});

    },function(error){
    	client.quit();
   throw new Errot(error);
  });




	return deffered.promise;
}

}

module.exports = MangaCacher;
