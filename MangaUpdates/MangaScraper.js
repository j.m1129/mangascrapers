(function (Scraper) {

    var Q = require("q");
    var configs = require("./configs");
    var request = require("request");
    var cheerio = require("cheerio");
    /**
     *
     * @param {string} url - Url of the content
     * @param {object} options -
     *
     */
    Scraper.RequestContent = function (url) {
        var deffered = Q.defer();
        if (typeof url != "string") deffered.reject("MangaId not provided");

        request({
            url: url,
            setTime: 3000
        }, function (err, response, body) {
            if (err)
                deffered.reject(err);
            else {
                if (response.statusCode == 200) {
                    deffered.resolve(body.toString());
                }
            }

        });


        return deffered.promise;
    };

    /**
     *Get Manga info by id
     * @param {Number} mangaID
     */
    Scraper.GetMangaByID = function (mangaID, options) {
        var deffered = Q.defer();
        if (typeof mangaID != "number") {
            deffered.reject("MangaId not provided");
        } else {
            var url = configs.MangaInfoUrl + mangaID;
            Scraper.RequestContent(url).then(function (domObject) {


                var dom = {
                    id: mangaID,
                    body: domObject
                };

                if(typeof options =="object" && options.hasOwnProperty("advanced") && options.advanced == true){
                    CreateMangaModelAdvanced(dom).then(function(mangaModel){
                        deffered.resolve(mangaModel);
                    }, function(err){
                        if(err)
                            deffered.reject(err);
                    })

                }else {
                    CreateMangaModel(dom).then(function (mangaModel) {

                        deffered.resolve(mangaModel);

                    }, function (err) {
                        if (err)
                            deffered.reject(err);
                    })
                }

            }, function (err) {
                if (err)
                    deffered.reject(err);
            })

        }
        return deffered.promise;
    };


    /**
     * Search Manga by title
     * @param {string} title
     * @param {object} options
     * @returns {promise|*|Q.promise}
     * @constructor
     */
    Scraper.SearchManga = function(title,options){
        var deffered = Q.defer();

        if(typeof  title =="string"){
            var searchUrl;
            if(typeof options =="object" && options.hasOwnProperty("page")){
                searchUrl = configs.getSearchUrl+title+"&page="+options.page;

            }else {
                searchUrl = configs.getSearchUrl+title;
            }

            Scraper.RequestContent(searchUrl).then(function(searchDom){
                CreateSearchModel(searchDom).then(function(searchModel){
                    deffered.resolve(searchModel);
                },function(err){
                    deffered.reject(err);
                })

            },function(err){
                deffered.reject(err);
            })



        }else {
            deffered.reject("Manga Title not provided");
        }

        return deffered.promise;
    }



    /**
     * Private functions
     */

    /**
     * Parse dom object and create manga model
     * @param domObject - Object contained scraped content of manga
     * @returns {Object} Manga model
     */
    var CreateMangaModel = function (domObject) {
        var deffered = Q.defer();

        if (typeof(domObject) == "undefined") {
            deffered.reject(new Error());

        } else if (typeof(domObject.id) == "undefined") {
            deffered.reject(new Error());
        } else if (typeof(domObject.body) == "undefined") {
            deffered.reject(new Error());
        } else {

            //Load html content
            var $ = cheerio.load(domObject.body);

            var title,
                type,
                description,
                genres = [],
                status,
                completed_scanlated,
               // groups_scanlating = [],
                img,
                authors,
                artists,
                year,
                //user_ratings,
                //anime_adaption,
                //related_series = [],
               // associated_names;

            //Retrieve the manga title
            title = $(".releasestitle.tabletitle").text();

            $(".sContainer").each(function (key, seriesInfo) {
                if (key == 0) {
                    $(seriesInfo).find(".sContent").each(function (key, info) {
                        //Description
                        if (key == 0) {
                            description = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }

                        //Type
                        if (key == 1) {
                            type = $(info).text().replace(/(\r\n|\n|\r)/gm, "");

                        }

                        //Related series
                        /*if (key == 2) {
                            $(info).find("a").each(function (key, related_info) {
                                var rel = {
                                    id: $(related_info).attr("href").split("?id=")[1],
                                    title: $(related_info).text()
                                };

                                related_series.push(rel);

                            });
                        }*/

                        //Associated names
                        if (key == 3) {

                        }

                        //Groups scanlating
                       /* if (key == 4) {
                            $(info).find("a").each(function (key, group_info) {

                                var urlId = $(group_info).attr("href").split("?id=")[1];

                                if (urlId != undefined) {

                                    var gr = {
                                        id: urlId,
                                        title: $(group_info).text()
                                    };

                                    groups_scanlating.push(gr);
                                }
                            });
                        }*/

                        //Status in country of origin
                        if (key == 6) {
                            var status_info = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                            if (status_info.toString().indexOf("Ongoing") != -1) {
                                status = "Ongoing";
                            } else {
                                status = "Completed";
                            }
                        }

                        //Completed scanlated
                        if (key == 7) {
                            var completed = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                            if (completed.toString().indexOf("Yes") != -1) {
                                completed_scanlated = true;
                            } else {
                                completed_scanlated = false;
                            }
                        }

                        //Anime adaptions
                       /* if (key == 8) {
                            anime_adaption = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }*/

                        //User ratings
                        /*if (key == 11) {
                            user_ratings = $(info).html().split("<br>")[0];
                        }*/
                    });
                }

                if (key == 1) {
                    $(seriesInfo).find(".sContent").each(function (key, info) {
                        // Manga Image
                        if (key == 0) {
                            img = $(info).find("img").attr("src");
                        }

                        //Genres
                        if (key == 1) {
                            $(info).find("a").each(function (key, genre_info) {
                                if ($(genre_info).text().indexOf("Search") == -1) {
                                    genres.push($(genre_info).text());
                                }
                            });
                        }

                        //Authors
                        if (key == 5) {
                            authors = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }

                        //Artists
                        if (key == 6) {
                            artists = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }

                        //Year
                        if (key == 7) {
                            year = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }

                    });
                }
            });

            var Series = {
                Id: domObject.id,
                Title: title,
                Description: description,
                Type: type,
                //Related_Series: related_series,
                //Groups_Scanlating: groups_scanlating,
                Status_in_origin: status,
                Completed_Scanned: completed_scanlated,
                //Anime_Adaption: anime_adaption,
                //User_Ratings: user_ratings,
                Image: img,
                Genres: genres,
                Authors: authors,
                Artists: artists,
                Year: year
            };
            deffered.resolve(Series);
        }


        return deffered.promise;

    }

    var CreateMangaModelAdvanced = function(domObject){
        var deffered = Q.defer();

        if (typeof(domObject) == "undefined") {
            deffered.reject(new Error());

        } else if (typeof(domObject.id) == "undefined") {
            deffered.reject(new Error());
        } else if (typeof(domObject.body) == "undefined") {
            deffered.reject(new Error());
        } else {

            //Load html content
            var $ = cheerio.load(domObject.body);

            var title,
                type,
                description,
                genres = [],
                status,
                completed_scanlated,
                groups_scanlating = [],
                img,
                authors,
                artists,
                year,
                user_ratings,
                anime_adaption,
                related_series = [],
                associated_names;

            //Retrieve the manga title
            title = $(".releasestitle.tabletitle").text();

            $(".sContainer").each(function (key, seriesInfo) {
                if (key == 0) {
                    $(seriesInfo).find(".sContent").each(function (key, info) {
                        //Description
                        if (key == 0) {
                            description = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }

                        //Type
                        if (key == 1) {
                            type = $(info).text().replace(/(\r\n|\n|\r)/gm, "");

                        }

                        //Related series
                        if (key == 2) {
                            $(info).find("a").each(function (key, related_info) {
                                var rel = {
                                    id: $(related_info).attr("href").split("?id=")[1],
                                    title: $(related_info).text()
                                };

                                related_series.push(rel);

                            });
                        }

                        //Associated names
                        if (key == 3) {

                        }

                        //Groups scanlating
                        if (key == 4) {
                            $(info).find("a").each(function (key, group_info) {

                                var urlId = $(group_info).attr("href").split("?id=")[1];

                                if (urlId != undefined) {

                                    var gr = {
                                        id: urlId,
                                        title: $(group_info).text()
                                    };

                                    groups_scanlating.push(gr);
                                }
                            });
                        }

                        //Status in country of origin
                        if (key == 6) {
                            var status_info = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                            if (status_info.toString().indexOf("Ongoing") != -1) {
                                status = "Ongoing";
                            } else {
                                status = "Completed";
                            }
                        }

                        //Completed scanlated
                        if (key == 7) {
                            var completed = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                            if (completed.toString().indexOf("Yes") != -1) {
                                completed_scanlated = true;
                            } else {
                                completed_scanlated = false;
                            }
                        }

                        //Anime adaptions
                        if (key == 8) {
                            anime_adaption = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }

                        //User ratings
                        if (key == 11) {
                            user_ratings = $(info).html().split("<br>")[0];
                        }
                    });
                }

                if (key == 1) {
                    $(seriesInfo).find(".sContent").each(function (key, info) {
                        // Manga Image
                        if (key == 0) {
                            img = $(info).find("img").attr("src");
                        }

                        //Genres
                        if (key == 1) {
                            $(info).find("a").each(function (key, genre_info) {
                                if ($(genre_info).text().indexOf("Search") == -1) {
                                    genres.push($(genre_info).text());
                                }
                            });
                        }

                        //Authors
                        if (key == 5) {
                            authors = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }

                        //Artists
                        if (key == 6) {
                            artists = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }

                        //Year
                        if (key == 7) {
                            year = $(info).text().replace(/(\r\n|\n|\r)/gm, "");
                        }

                    });
                }
            });

            var Series = {
                Id: domObject.id,
                Title: title,
                Description: description,
                Type: type,
                Related_Series: related_series,
                Groups_Scanlating: groups_scanlating,
                Status_in_origin: status,
                Completed_Scanned: completed_scanlated,
                Anime_Adaption: anime_adaption,
                User_Ratings: user_ratings,
                Image: img,
                Genres: genres,
                Authors: authors,
                Artists: artists,
                Year: year
            };
            deffered.resolve(Series);
        }


        return deffered.promise;

    }

    /**
     * Parse searched dom object and create search model
     * @param domObject - Object container scraped search content
     * @returns {promise|*|Q.promise}
     */
    var CreateSearchModel = function(domObject){
        var deffered = Q.defer();
        if(typeof domObject =="undefined"){
            deffered.reject("HtmlContent not provided");
        }else {
            //Load the html contain using cheerio
            var $ = cheerio.load(domObject.toString());
            //The return object
            var SearchResult = {};
            /*
             * Retrieve pagination info. Current page and total pages
             */
            $(".specialtext").each(function(key, value) {

                if (key == 3) {

                    var currentpage = parseInt($(value).find("font").text());
                    var pagesString = $(value).text().split("[")[0].split("(")[1].split(")")[0];
                    var totalpages = parseInt(pagesString);
                    var pagination = {
                        total: totalpages,
                        current: currentpage
                    };

                    SearchResult.Pagination = pagination;
                }
            })

            //Retrieve series info
            var series = [];
            $(".text.pad.col1").each(function(key, results) {

                var url = $(results).find("a").attr("href");
                if(typeof(url)!="undefined"){

                    var urlsplit = url.split("?id=");
                    var id = urlsplit[1];

                    /* Get mangainfo here using the id
                     * Return a json array with mangainfo
                     */
                    series.push(id);

                }


            })

            SearchResult.series = series;

            deffered.resolve(SearchResult);

        }

        return deffered.promise;
    }

})(module.exports);
