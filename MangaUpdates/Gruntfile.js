module.exports = function(grunt){
    //Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

        nodemon:{
            dev:{
                script:"test/MangaScraper_test.js"
            }
        },
        mochaTest:{
            options:{
                reporter: "spec",
                timeout:60000

            },
            src: ["test/**/*.js"]
        },
        jsdoc: {
            dist: {
                src: ["*.js"],
                options:{
                    destination: "doc"
                }
            }
        },
        concurrent: {
            target:{
                task:["mochaTest"]
            }
        },
        watch:{
            scripts: {
                files:["**/*.js"],
                tasks:["mochaTest"],
                options:{
                    spawn:false
                }
            }
        }


    });

    //Load modules
    //Load modules
    grunt.loadNpmTasks("grunt-nodemon");
    grunt.loadNpmTasks("grunt-mocha-test");
    grunt.loadNpmTasks("grunt-jsdoc");
    grunt.loadNpmTasks("grunt-contrib-watch");
    //Register tasks
    grunt.registerTask("default",["nodemon"]);
    grunt.registerTask("test",["mochaTest"]);
    grunt.registerTask("doc",["jsdoc"]);



};
